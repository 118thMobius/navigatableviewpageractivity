package org.mobiusone.mobile.navigatableviewpageractivity;

import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends NavigatableViewPagerActivity {

    @Override
    public Fragment getItem(int position) {
        return new PlaceholderFragment();
    }

    @Override
    public void inflateBottomNavigationViewMenu(BottomNavigationView bottomNavigation) {
        bottomNavigation.inflateMenu(R.menu.navigation);
    }
}
