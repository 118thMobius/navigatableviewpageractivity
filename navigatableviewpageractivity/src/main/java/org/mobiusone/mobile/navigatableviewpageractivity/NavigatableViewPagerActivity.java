package org.mobiusone.mobile.navigatableviewpageractivity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.io.Serializable;

public abstract class NavigatableViewPagerActivity extends AppCompatActivity {
    protected ViewPager mViewPager;
    protected BottomNavigationView mBottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigatable_view_pager);

        mViewPager = findViewById(R.id.container);
        mBottomNavigation = findViewById(R.id.navigation);

        mViewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
        mViewPager.addOnPageChangeListener(new MyOnPageChangeListener());

        inflateBottomNavigationViewMenu(mBottomNavigation);
        mBottomNavigation.setOnNavigationItemSelectedListener(new MyOnNavigationItemSelectedListener());
    }


    public abstract void inflateBottomNavigationViewMenu(BottomNavigationView bottomNavigation);

    public abstract Fragment getItem(int position);

    public class MyOnNavigationItemSelectedListener implements BottomNavigationView.OnNavigationItemSelectedListener {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            for (int index = 0; index < mBottomNavigation.getMenu().size(); index++) {
                if (item.equals(mBottomNavigation.getMenu().getItem(index))) {
                    mViewPager.setCurrentItem(index);
                    return true;
                }
            }
            return false;
        }
    }

    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            int id = mBottomNavigation.getMenu().getItem(position).getItemId();
            mBottomNavigation.setSelectedItemId(id);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return NavigatableViewPagerActivity.this.getItem(position);
        }

        @Override
        public int getCount() {
            return 3;
        }
    }


    public static class PlaceholderFragment extends Fragment {
        private final static String CONTENT_TAG = "content";

        private FrameLayout mContainer;
        private View mProgress;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance() {
            return new PlaceholderFragment();
        }

        public void setContent(Fragment fragment) {
            getChildFragmentManager().beginTransaction().replace(mContainer.getId(), fragment, CONTENT_TAG).commit();
            mProgress.setVisibility(View.GONE);
        }

        public Fragment getContent() {
            return getChildFragmentManager().findFragmentByTag(CONTENT_TAG);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_placeholder, container, false);
            mContainer = rootView.findViewById(R.id.container);
            mProgress = rootView.findViewById(R.id.progress);
            return rootView;
        }
    }

    public abstract static class AutomaticPlaceholderFragment<D extends Serializable> extends PlaceholderFragment {
        private static final String ARGS = "args";

        D[] args = null;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (savedInstanceState != null && savedInstanceState.containsKey(ARGS)){
                args = (D[]) savedInstanceState.getSerializable(ARGS);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = super.onCreateView(inflater, container, savedInstanceState);

            Fragment content = getContent();
            if (content != null) {
                //すでにContentが存在していたら、それを復元する
                setContent(content);
            } else if (args != null) {
                //Contentは存在していないが、argsはすでに存在している場合、argsを用いてContentを作成して表示
                setContent(getContentFragment(args));
            }
            //何も存在していなければ、そのまま
            return view;
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            outState.putSerializable(ARGS,args);
            super.onSaveInstanceState(outState);
        }

        abstract protected Fragment getContentFragment(D[] args);

        @SafeVarargs
        public final void setContentArgs(D... args) {
            this.args = args;
            setContent(getContentFragment(args));
        }
    }

}
